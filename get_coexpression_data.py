__author__ = 'justingibbons'

import pandas as pd
import numpy as np
import csv

class CoexpressionDataframe:
    def __init__(self,infile,calculate_coexpresion=True,transpose_data=True,correlation_method="kendall",sep="\t"):
        """Methods to work with Coexpression dataframe. If calculate_coexpression is True then a co-expression
                dataframe is calculated, otherwise it is assumed that the input is a file containing a co-expression
                        matrix. If transpose_data is True then the data is transposed before calculating the
                                coexpression dataframe. NaN are converted to zero"""
        if calculate_coexpresion==True:
            dataframe=pd.read_csv(infile,header=0,index_col=0,sep=sep)
            dataframe.fillna(0,inplace=True) #Replace NaN with zero
            if transpose_data==True:
                dataframe=dataframe.transpose()
            self.coexpression_matrix=dataframe.corr(method=correlation_method)

            #print self.coexpression_matrix.index
        else:
            self.coexpression_matrix=pd.read_csv(infile,header=0,index_col=0,sep=sep)

    def __str__(self):
        return self.coexpression_matrix.to_string()


    def write_out_pairs_with_min_absolute_corr(self,outfile,min_abs_corr=0,sep="\t",round_to=2):
        """Writes out the correlation data to a file in the form of rowlabel \t columnlabel \t correlation if
        the absoulte value of the correlation is at or above min_abs_corr. Automatically filters out self correlation"""

        with open(outfile,"w") as  out:
            csv_writer=csv.writer(out,delimiter=sep)

            for row in self.coexpression_matrix.index:
                for column in self.coexpression_matrix.columns:
                    if row==column:
                        continue
                    corr_value=round(self.coexpression_matrix[row][column],round_to)
                    if abs(corr_value)<min_abs_corr or np.isnan(corr_value):
                        continue
                    else:
                        write_row=[row,column,corr_value]
                        csv_writer.writerow(write_row)

    def write_out_matrix(self,outfile,sep="\t"):
        self.coexpression_matrix.to_csv(outfile,sep=sep)

    def random_sample(self,n):
        """Returns a random sample of n rows from the correlation dataframe"""

        return self.coexpression_matrix.sample(n)




if __name__=="__main__":
    import unittest
    from compare_files_tools import get_file_lines
    from pandas.util.testing import assert_frame_equal
    class TestCoexpressionDataframe(unittest.TestCase):
        def setUp(self):
            self.infile="test_files/infiles/sample_rna_expression.txt"
            self.corr_infile="test_files/infiles/sample_corr_data.txt"

        def test_coexpression_matrix_creation_kendall(self):
            ref_file="test_files/ref_files/ref_kendall_corr.txt"
            result_outfile="test_files/outfiles/output_dataframe_kendall_corr.txt"

            result_df=CoexpressionDataframe(self.infile)
            #write the result to file for easy comparison
            result_df.coexpression_matrix.to_csv(result_outfile,sep="\t")
            result_lines,ref_lines=get_file_lines(result_outfile,ref_file)
            self.assertEqual(result_lines,ref_lines)

        def test_coexpression_matrix_spearman(self):
            ref_file="test_files/ref_files/ref_spearman_corr.txt"
            result_outfile="test_files/outfiles/output_dataframe_spearman_corr.txt"
            result_df=CoexpressionDataframe(self.infile,correlation_method="spearman")
            result_df.coexpression_matrix.to_csv(result_outfile,sep="\t")
            result_lines,ref_lines=get_file_lines(result_outfile,ref_file)
            self.assertEqual(result_lines,ref_lines)


        def test_write_out_matrix(self):
            ref_file="test_files/ref_files/ref_kendall_corr.txt"
            result_outfile="test_files/outfiles/output_write_out_matrix.txt"

            result_df=CoexpressionDataframe(self.infile)
            result_df.write_out_matrix(result_outfile)

            result,ref=get_file_lines(result_outfile,ref_file)
            self.assertEqual(result,ref)

        # def test_load_already_existing_corr_data(self):
        #     test_obj=CoexpressionDataframe(self.corr_infile,calculate_coexpresion=False)
        #     ref_obj=CoexpressionDataframe(self.infile)
        #     #Test is failing because when calc matrix the columns loss their name attribute
        #     assert_frame_equal(test_obj.coexpression_matrix,ref_obj.coexpression_matrix)

        def test_write_out_pairs_with_min_absolute_corr_created_corr_matrix(self):
            result_file="test_files/outfiles/output_write_out_pairs_with_min_absolute_corr_created_corr_matrix.txt"
            ref_file="test_files/ref_files/ref_write_out_pairs_with_min_absolute_corr_create_matrix.txt"
            result_df=CoexpressionDataframe(self.infile)
            result_df.write_out_pairs_with_min_absolute_corr(result_file,min_abs_corr=0.4)
            result,ref=get_file_lines(result_file,ref_file)
            self.assertEqual(result,ref)

        def test_write_out_pairs_with_min_abolute_corr_read_in_corr_dataframe(self):
            result_file="test_files/outfiles/output_write_out_pairs_with_min_absolute_corr_read_in_corr.txt"
            #The result should be the same for as with the calculated corr dataframe
            ref_file="test_files/ref_files/ref_write_out_pairs_with_min_absolute_corr_create_matrix.txt"
            result_df_obj=CoexpressionDataframe(self.corr_infile,calculate_coexpresion=False)
            result_df_obj.write_out_pairs_with_min_absolute_corr(result_file,min_abs_corr=0.4)

            result,ref=get_file_lines(result_file,ref_file)
            self.assertEqual(result,ref)



    unittest.main()